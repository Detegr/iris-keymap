#include "iris.h"
#include "action_layer.h"
#include "eeconfig.h"
#include <stdint.h>

extern keymap_config_t keymap_config;

enum custom_keycodes {
	KC_LOWR = SAFE_RANGE,
	KC_RASE,
	KC_NUMB,
};

enum {
	TD_OPEN_TERMINAL,
	TD_ALT_SHIFT,
};

#define _COLEMAK 0
#define _NUMPAD 1
#define _LOWER 2
#define _RAISE 3
#define _NUMBERS 4

#define KC_ KC_TRNS
#define _______ KC_TRNS
#define KC_XXXX KC_NO
#define KC_NUMP TG(_NUMPAD)
#define KC_TERM LCTL(LALT(KC_V))
#define KC_SINS LSFT(KC_INS)
#define KC_AE 0x64
#define KC_OE LSFT(0x64)
//#define KC_RASE LT(_RAISE, KC_ENT)
#define KC_RASE MO(_RAISE)
#define KC_LOWR MO(_LOWER)
#define KC_NUMB MO(_NUMBERS)
#define KC_OPEN_TERMINAL LGUI(KC_ENT)
#define KC_Lgui TD(TD_OPEN_TERMINAL)
#define KC_Lsft TD(TD_ALT_SHIFT)

qk_tap_dance_action_t tap_dance_actions[] = {
	[TD_OPEN_TERMINAL] = ACTION_TAP_DANCE_DOUBLE(KC_LGUI, KC_OPEN_TERMINAL),
	[TD_ALT_SHIFT] = ACTION_TAP_DANCE_DOUBLE(KC_LSFT, KC_LALT),
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

 [_COLEMAK] = KC_KEYMAP(
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     ESC , 1  , 2  , 3  , 4  , 5  ,                6  , 7  , 8  , 9  , 0  ,EQL ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     TAB , Q  , W  , F  , P  , G  ,                J  , L  , U  , Y  ,SCLN,TERM,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
     BSPC, A  , R  , S  , T  , D  ,                H  , N  , E  , I  , O  ,QUOT,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
     Lsft, Z , X  , C  , V  , B  ,NUMP,      ENT , K  , M  ,COMM, DOT,SLSH,RSFT,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       LCTL,LOWR,SPC ,         ENT,RASE,Lgui
  //                  `----+----+----'        `----+----+----'
  ),

  [_NUMPAD] = KC_KEYMAP( 
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
     F1  , F2 , F3 , F4 , F5 , F6 ,                F7 , F8 , F9 ,F10 ,F11 ,F12 ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,    ,BTN2,MS_U,BTN1,    ,                   , P7 , P8 , P9 ,    ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,    ,MS_L,MS_D,MS_R,    ,                   , P4 , P5 , P6 ,    ,    ,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,    ,     PENT,DOT , P1 , P2 , P3 , P0 ,    ,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                       LALT,LOWR,BSPC,             , P0 ,NLCK
  //                  `----+----+----'        `----+----+----'
  ),


  [_LOWER] = KC_KEYMAP( 
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
         ,MUTE,VOLD,VOLU,    ,    ,                   ,    ,    ,LPRN,RPRN,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,CAPS,PGUP, UP ,PGDN, ESC,                   ,MINS,UNDS,LBRC,RBRC,SINS,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
      DEL,HOME,LEFT,DOWN,RGHT, END,                   ,    ,    ,    ,PIPE,    ,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,    ,         ,    ,    ,    ,    ,BSLS,    ,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                           ,    ,   ,            ,NUMB,    
  //                  `----+----+----'        `----+----+----'
  ),

  [_RAISE] = KC_KEYMAP( 
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
      GRV,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,                   , 7  , 8  , 9  ,    ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         , AE ,    ,    ,    ,    ,                   , 4  , 5  , 6  , OE ,    ,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,    ,         ,    , 1  , 2  , 3  , 0  ,    ,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                           ,NUMB,    ,             ,    ,    
  //                  `----+----+----'        `----+----+----'
  ),

  [_NUMBERS] = KC_KEYMAP( 
  //,----+----+----+----+----+----.              ,----+----+----+----+----+----.
         ,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,                   ,    ,    ,    ,    ,    ,
  //|----+----+----+----+----+----|              |----+----+----+----+----+----|
         , 1  , 2  , 3  , 4  , 5  ,                6  , 7  , 8  , 9  , 0  ,    ,
  //|----+----+----+----+----+----+----.    ,----|----+----+----+----+----+----|
         ,    ,    ,    ,    ,    ,    ,         ,    ,    ,    ,    ,    ,    ,
  //`----+----+----+--+-+----+----+----/    \----+----+----+----+----+----+----'
                           ,    ,    ,             ,    ,    
  //                  `----+----+----'        `----+----+----'
  ),
};

void persistent_default_layer_set(uint16_t default_layer) {
  eeconfig_update_default_layer(default_layer);
  default_layer_set(default_layer);
}
